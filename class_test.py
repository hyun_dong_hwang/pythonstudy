#!/usr/bin/python
# -*- coding: utf-8 -*-



class MyClass(object):
    
    """docstring for MyClass"""

    def __init__(self, a, b, c):
        super(MyClass, self).__init__()
        self.a = a
        self.b = b
        self.c = c
        pass

    def add(self):
        self.c = self.a + self.b
        pass

    def printC(self):
        print("self.c : %d" % (self.c))
        pass

pass



obj = MyClass(1, 2, 0)
print(obj)
obj.add()
obj.printC()

obj2 = object()
print(obj2)