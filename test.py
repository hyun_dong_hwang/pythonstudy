#!/usr/bin/python
# -*- coding: utf-8 -*-

import httplib
from AppKit import NSPasteboard, NSArray

pb = NSPasteboard.generalPasteboard()
pb.clearContents()
a = NSArray.arrayWithObject_("hello world")
pb.writeObjects_(a)

a = "hello"
print(a)
b = a.split("e")
print(b)
a = 1
c = []

json = { "a":1}
json[a] = b
print(json)

conn = httplib.HTTPConnection("www.naver.com")
conn.request("GET", "");
res = conn.getresponse();
print(res)
print(res.status)
print(res.reason)
resData = res.read()
print(len(resData))
print(resData)


# simple.py
# languages = ['python', 'perl', 'c', 'java']

# for lang in languages:
#     if lang in ['python', 'perl']:
#         print("%6s need interpreter" % lang)
#     elif lang in ['c', 'java']:
#         print("%6s need compiler" % lang)
#     else:
#         print("should not reach here")