#!/usr/bin/python
# -*- coding: utf-8 -*-

a = 1
b = "hello"

print("""
    a : %d
    b : %s
    """
    %
    (a , b)
)

c = "hobby"
print(c.upper())
print(c.count("b"))
print(c.find("b"))
print(c.index("b"))